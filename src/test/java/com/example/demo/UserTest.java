package com.example.demo;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.WritingRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserTest {


//    @Test
//    public void shouldFindById() {
//        User user = new User();
//        user.setLogin("test@example.com");
//        user.setName("John Doe");
//        user.setAge(1);
//        user.setRole("standard");
//        user.setId(0);
//        userRepository.save(user);
//        assertEquals(user, findById(0));
//        userRepository.deleteById(0);
//    }

    @Test
    public void shouldCheckAdminRole() {
        User user = new User();
        assertTrue(!user.isAdmin());
        user.setRole("root");
        assertTrue(user.isAdmin());
    }

}

