package com.example.demo.service;

import com.example.demo.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.Date;

public class JwtUtil {

    private static final long EXPIRATION_TIME = 3600000;
    private static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public static String generateToken(User user) {
        Date currentTime = new Date();
        return Jwts.builder()
                .setId(user.getId().toString())
                .setSubject(user.getLogin())
                .setIssuedAt(currentTime)
                .setExpiration(new Date(currentTime.getTime() + EXPIRATION_TIME))
                .signWith(SECRET_KEY)
                .compact();
    }


    public static Integer parseToken(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token)
                    .getBody();
            return Integer.parseInt(claims.getId());
        } catch (io.jsonwebtoken.security.SignatureException e) {
            return -1;
        }
    }

}
