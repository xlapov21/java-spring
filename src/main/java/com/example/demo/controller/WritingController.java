package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.model.Writing;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.WritingRepository;
import com.example.demo.service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@Controller
public class WritingController {


    @GetMapping("/writings")
    public String writings(@RequestParam("token") String token, Model model) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            List<Writing> writings = writingRepository.findByUserId(user.getId());
            model.addAttribute("writings", writings);
            model.addAttribute("newWriting", new Writing());
            return "writings";
        } else {
            return "redirect:/login";
        }
    }

    @PostMapping("/writings")
    public String createWriting(@RequestParam("token") String token, @RequestParam("title") String title,
                                @RequestParam("description") String description, Model model) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            Writing newWriting = new Writing(title, description);
            newWriting.setUserId(user.getId());
            writingRepository.save(newWriting);
            List<Writing> writings = writingRepository.findByUserId(user.getId());
            model.addAttribute("writings", writings);
            model.addAttribute("newWriting", new Writing());
            return "redirect:writings?token=" + token;
        } else {
            return "redirect:/login";
        }
    }

    @GetMapping("/writing/update")
    public String updateWriting(@RequestParam("token") String token, @RequestParam("title") String title,
                                @RequestParam("description") String description, @RequestParam("id") Integer id, Model model) {

        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            Writing writing = writingFindById(id);
                if (writing != null && writing.getUserId().equals(user.getId())) {
                    writing.setTitle(title);
                    writing.setDescription(description);
                    writingRepository.save(writing);
                    List<Writing> writings = writingRepository.findByUserId(user.getId());
                    model.addAttribute("writings", writings);
                    model.addAttribute("newWriting", new Writing());
                    return "redirect:/writings?token=" + token;
                } else {
                    return "redirect:/writings?token=" + token;
                }

        } else {
            return "redirect:/login";
        }
    }

    @GetMapping("/writing/delete")
    public String deleteWriting(@RequestParam("token") String token, @RequestParam("id") Integer id, Model model) {

        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            Writing writing = writingFindById(id);
            if (writing != null && writing.getUserId().equals(user.getId())) {
                writingRepository.deleteById(id);
                List<Writing> writings = writingRepository.findByUserId(user.getId());
                model.addAttribute("writings", writings);
                model.addAttribute("newWriting", new Writing());
                return "redirect:/writings?token=" + token;
            } else {
                return "redirect:/writings?token=" + token;
            }

        } else {
            return "redirect:/login";
        }
    }



    @Autowired
    UserRepository userRepository;

    @Autowired
    WritingRepository writingRepository;

    public User findById(Integer id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return userData.get();
        } else {
            return null;
        }
    }
    public Writing writingFindById(Integer id) {
        Optional<Writing> writingData = writingRepository.findById(id);
        if (writingData.isPresent()) {
            return writingData.get();
        } else {
            return null;
        }
    }


}
