package com.example.demo.controller;

import com.example.demo.model.LoginDTO;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class ApiUserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginDTO loginDto) {
        User user = authenticate(loginDto.getLogin(), loginDto.getPassword());
        if (user != null) {
            String token = JwtUtil.generateToken(user);
            return new ResponseEntity<>(token, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }
    }


//    @PostMapping("/registration")
//    public String processRegistration(@ModelAttribute("user") User user, Model model) {
//        user.setRole("standard");
//        userRepository.save(user);
//        String token = JwtUtil.generateToken(user);
//        return "redirect:/profile?token=" + token;
//    }

    private User authenticate(String login, String password) {
        List<User> userList = userRepository.findByLogin(login);
        if (userList.size() > 0) {
            User user = userList.get(0);
            if (user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public User findById(Integer id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return userData.get();
        } else {
            return null;
        }
    }
}
