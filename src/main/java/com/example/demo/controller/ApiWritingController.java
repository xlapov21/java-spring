package com.example.demo.controller;

import com.example.demo.model.User;
import com.example.demo.model.Writing;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.WritingRepository;
import com.example.demo.service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class ApiWritingController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    WritingRepository writingRepository;

    @GetMapping("/writings/{id}")
    public ResponseEntity<Writing> getWritingById(@RequestHeader("Token") String token, @PathVariable("id") Integer id) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            Optional<Writing> writingData = writingRepository.findById(id);
            if (writingData.isPresent() && writingData.get().getUserId().equals(user.getId())) {
                return new ResponseEntity<>(writingData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/writings")
    public ResponseEntity<Writing> createWriting(@RequestHeader("Token") String token, @RequestBody Writing writing) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            try {
                Writing _writing = writingRepository
                        .save(new Writing(writing.getTitle(), writing.getDescription(), user.getId()));
                return new ResponseEntity<>(_writing, HttpStatus.CREATED);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/writings/{id}")
    public ResponseEntity<Writing> updateWriting(@RequestHeader("Token") String token, @PathVariable("id") Integer id, @RequestBody Writing writing) {
        User user = findById(JwtUtil.parseToken(token));

        if (user != null) {
            Optional<Writing> writingData = writingRepository.findById(id);

            if (writingData.isPresent() && writingData.get().getUserId().equals(user.getId())) {
                Writing _writing = writingData.get();
                _writing.setTitle(writing.getTitle());
                _writing.setDescription(writing.getDescription());
                return new ResponseEntity<>(writingRepository.save(_writing), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/writings/{id}")
    public ResponseEntity<HttpStatus> deleteWriting(@RequestHeader("Token") String token, @PathVariable("id") Integer id) {
        User user = findById(JwtUtil.parseToken(token));

        if (user != null) {
            Optional<Writing> writingData = writingRepository.findById(id);

            if (writingData.isPresent() && writingData.get().getUserId().equals(user.getId())) {
                try {
                    writingRepository.deleteById(id);
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }

    public User findById(Integer id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return userData.get();
        } else {
            return null;
        }
    }
}
