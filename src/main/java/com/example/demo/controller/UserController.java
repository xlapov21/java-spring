package com.example.demo.controller;

import com.example.demo.model.LoginDTO;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8080")
@Controller
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("loginDto", new LoginDTO());
        return "login";
    }

    @PostMapping("/login")
    public String processLogin(@ModelAttribute("loginDto") LoginDTO loginDto, Model model) {
        User user = authenticate(loginDto.getLogin(), loginDto.getPassword());
        if (user != null) {
            String token = JwtUtil.generateToken(user);
            return "redirect:/profile?token=" + token;
        } else {
            model.addAttribute("error", "Invalid login or password");
            return "login";
        }
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("user", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String processRegistration(@ModelAttribute("user") User user, Model model) {
        user.setRole("standard");
        userRepository.save(user);
        String token = JwtUtil.generateToken(user);
        return "redirect:/profile?token=" + token;
    }


    @GetMapping("/profile")
    public String profile(@RequestParam("token") String token, Model model) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            model.addAttribute("user", user);
            return "profile";
        } else {
            return "redirect:/login";
        }
    }


    @GetMapping("/users")
    public String getAllUsers(@RequestParam("token") String token, Model model) {
        User user = findById(JwtUtil.parseToken(token));
        if (user != null) {
            if (user.getRole().equals("root")) {
                List<User> users = userRepository.findAll();
                model.addAttribute("users", users);
                model.addAttribute("token", token);
                return "users";
            } else {
                return "redirect:/profile?token=" + token;
            }
        } else {
            return "redirect:/login";
        }
    }

    private User authenticate(String login, String password) {
        List<User> userList = userRepository.findByLogin(login);
        if (userList.size() > 0) {
            User user = userList.get(0);
            if (user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public User findById(Integer id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return userData.get();
        } else {
            return null;
        }
    }

}
