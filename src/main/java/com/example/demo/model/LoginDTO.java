package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDTO {
    private String login;
    private String password;
}
