package com.example.demo.model;


import com.example.demo.repository.UserRepository;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Setter
@Getter
@Entity
@Table(name = "users")
public class User {

    @Autowired
    static UserRepository userRepository;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    public User() {

    }

    public User(String name, Integer age, String login, String password, String role) {
        this.name = name;
        this.age = age;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public static User findById(Integer id) {
        Optional<User> userData = userRepository.findById(id);
        if (userData.isPresent()) {
            return userData.get();
        } else {
            return null;
        }
    }

    public boolean isAdmin() {
        return role != null && role.equals("root");
    }


    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name + "]";
    }
}
