package com.example.demo.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "writings")
public class Writing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "description")
    private String description;

    public Writing() {

    }

    public Writing(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Writing(String title, String description, Integer userId) {
        this.title = title;
        this.description = description;
        this.userId = userId;

    }

}

