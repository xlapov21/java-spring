package com.example.demo.repository;

import java.util.List;

import com.example.demo.model.Writing;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WritingRepository extends JpaRepository<Writing, Integer> {
    List<Writing> findByUserId(Integer userId);

}
